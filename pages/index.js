import eventBus from '~/plugins/event-bus.js'
import sort from '~/components/sort/sort.vue'
import product from '~/components/product/product.vue'
import cart from '~/components/cart/cart.vue'

export default {
	name: 'listing',

	components: {
		sort,
		product,
		cart
	},

	data () {
		return {
			isLoading: true,
			sortType: 1,
			addedProducts: [],
			products: [
				{
					id: 312,
					name: "Super Mario Odyssey",
					price: 197.88,
					score: 100,
					image: "super-mario-odyssey.png"
				},
				{
					id: 201,
					name: "Call Of Duty Infinite Warfare",
					price: 49.99,
					score: 80,
					image: "call-of-duty-infinite-warfare.png"
				},
				{
					id: 102,
					name: "The Witcher III Wild Hunt",
					price: 119.5,
					score: 250,
					image: "the-witcher-iii-wild-hunt.png"
				},
				{
					id: 99,
					name: "Call Of Duty WWII",
					price: 249.99,
					score: 205,
					image: "call-of-duty-wwii.png"
				},
				{
					id: 12,
					name: "Mortal Kombat XL",
					price: 69.99,
					score: 150,
					image: "mortal-kombat-xl.png"
				},
				{
					id: 74,
					name: "Shards of Darkness",
					price: 71.94,
					score: 400,
					image: "shards-of-darkness.png"
				},
				{
					id: 31,
					name: "Terra Média: Sombras de Mordor",
					price: 79.99,
					score: 50,
					image: "terra-media-sombras-de-mordor.png"
				},
				{
					id: 420,
					name: "FIFA 18",
					price: 195.39,
					score: 325,
					image: "fifa-18.png"
				},
				{
					id: 501,
					name: "Horizon Zero Dawn",
					price: 115.8,
					score: 290,
					image: "horizon-zero-dawn.png"
				}
			]
		}
	},

	mounted() {
		this.showLoading()

		// eventBus.$on('removeItems', removeItems => { this.removedItemsCart() })

	},

	computed: {
		sortedArray(index) {
			let sortType = this.sortType
			function compare(a, b) {
				if (sortType === 1) {
		      if (a.name > b.name) {
		        return 1
					}
				}
				else if (sortType === 2) {
		      if (a.price > b.price) {
		        return 1
					}
				}	else {
				   if (a.score < b.score) {
		        return 1
					}
				}
			}
			return this.products.sort(compare)
		}
	},

	methods: {
		showLoading() {
			if (this.isLoading === false) {
				this.isLoading = true
				setTimeout(()=> {
					this.isLoading = false
		 		},500)
			} else {
				this.isLoading = false
			}
		},

		getSortType(sortType) {
			return this.sortType = sortType
		},

		addedItems(addedProducts) {
			this.addedProducts.push(addedProducts)
    	console.log(`ADDED: ${addedProducts.name} foi adicionado!`)
    },

		removedItems(addedProducts) {
			this.addedProducts = this.addedProducts.filter(v => v.id != addedProducts.id)
			console.log(`REMOVED: ${addedProducts.name}`)
		},

		// removedItemsCart(addedProducts) {
		// 	// console.log(this.addedProducts[0].id)
		// 	this.addedProducts = this.addedProducts.filter(v => v.id != this.addedProducts[0].id)
		// 	// console.log(`REMOVED`)
		// },

		openFilter() {
			return true
		}
 	}

}
