# cr-test

> Consulta Remedios Frontend Test

## Como rodar o projeto

``` bash
# Ter o node atualizado (versão usada v10.4.1)
# Instala todas dependências
$ npm i && npm audit fix

# Inicia o servidor na porta 3333. Ex.: http://localhost:3333
$ npm run dev
```

### Obervações
* Modifiquei um pouco a interface.
* Adicionei algumas microinterações.
* A largura mínima fixada foi de 1280px (conforme consta no layout).
* O botão de remover (no próprio carrinho) não funciona. Só o hover nos itens. Mas o botão na listagem funciona perfeitamente.

Testado apenas em: OSX Yosemite com o Chrome 67
