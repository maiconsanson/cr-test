import eventBus from '~/plugins/event-bus.js'

export default {
	name: 'product',

	data () {
		return {
			isAdded: 0,
			isRemoved: 1
		}
	},

	props: ['id', 'name', 'price', 'score', 'image'],

	methods: {
		addItem() {
			this.isAdded = true
			this.removed = false
			this.$emit('added', { 'id': this.id, 'name': this.name, 'price': this.price, 'image': this.image })
			eventBus.$emit('addItem')
		},

		removeItem() {
			this.isAdded = false
			this.isRemoved = true
			this.$emit('removed', { 'id': this.id, 'name': this.name, 'price': this.price })
			eventBus.$emit('removeItem')
		},

		hideActive() {
			this.isHide = false
			this.isActive = true
		}
	}

}
