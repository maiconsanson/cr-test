import eventBus from '~/plugins/event-bus.js'

export default {
	name: 'cart',

	data() {
		return {
			scrollPosition: 0,
			shippingBase: 10,
			shippingTotal: null,
			subtotal: null,
			total: null
		}
	},

	props: ['addedItems'],
	// props: ['id', 'name', 'price', 'score', 'image'],

	mounted () {
		eventBus.$on('addItem', addItem => { this.addItem() })
		eventBus.$on('removeItem', removeItem => { this.removeItem() })
		window.addEventListener('scroll', this.updateScroll);
	},

	methods: {
		addItem() {
			this.shippingTotal = this.shippingTotal + this.shippingBase
		},

		removeItem() {
			this.shippingTotal = this.shippingTotal - this.shippingBase
			// eventBus.$emit('removeItems', this.addedItems)
		},

		getSubtotal() {
		  let subtotal = []
		  Object.entries(this.addedItems).forEach(([key, val]) => {
	      subtotal.push(val.price)
		  })
		  return subtotal.reduce(function(subtotal, num) {
				return subtotal + num
			},0)
		},

		getTotal() {
			if (this.getSubtotal() >= 250) {
				return this.getSubtotal()
			} else {
				return this.getSubtotal() + this.shippingTotal
			}
		},

		freeShipping() {
			if (this.getSubtotal() >= 250) {
				return true
			}
		},

		itemsCounter() {
			let counter = this.addedItems.length
			if (counter === 0) {
				return
			} else if (counter === 1) {
				return `(${counter} item)`
			} else {
				return `(${counter} itens)`
			}
		},

		updateScroll() {
			this.scrollPosition = window.scrollY
		}
	}
}
