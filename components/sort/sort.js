export default {
	name: 'sort',

	data () {
		return {
			open: null,
			placeholder_name: '',
			itemIdActive: 1,
			items: [
				{
					id: 1,
					name: 'Ordem Alfabética'
				},
				{
					id: 2,
					name: 'Mais Baratos'
				},
				{
					id: 3,
					name: 'Mais Populares'
				}
			]
		}
	},

	methods: {
		openSort() {
			this.open = !this.open
		},

		selectItem(index) {
			let sortName = this.items[index].name
			let sortId = this.items[index].id
			this.placeholder_name = sortName
			this.$emit('sort', sortId)
			this.$emit('loading', true)
			this.itemIdActive = this.items[index].id
			console.log('SORTING: ' + sortName)
		}
	},

	mounted() {
		this.placeholder_name = this.items[0].name
		console.log('SORTING: ' + this.items[0].name)
  }
}
