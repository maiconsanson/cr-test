module.exports = {
  /*
  ** Headers of the page
  */
  head: {
		htmlAttrs: {
      lang: 'pt-BR',
    },
    title: 'cr-test',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Consulta Remedios Frontend Test' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{ rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },

	plugins: [
		'~/plugins/event-bus.js',
    '~/filters/to-currency.js'
  ],

  modules: [
    'nuxt-sass-resources-loader'
  ],
  sassResources: [
    '@/assets/scss/_constants.scss'
  ]
}
